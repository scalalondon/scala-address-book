package uk.co

import java.text.SimpleDateFormat

import org.joda.time.DateTimeZone

/**
 * Created by tony_murphy on 03/08/15.
 */
package object gumtree {

  val JODA_BRITISH_TIME_ZONE: DateTimeZone = DateTimeZone.forID("Europe/London")
  val ADDRESS_BOOK_CSV_NUMBER_PARTS: Int = 3
  val ADDRESS_BOOK_DATE_TIME_FORMATTER: SimpleDateFormat = new SimpleDateFormat("d/M/yy")
}
