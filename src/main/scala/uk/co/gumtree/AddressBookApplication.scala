package uk.co.gumtree

/**
 * Created by tony_murphy on 02/08/15.
 */
object AddressBookApplication {
  def main(args: Array[String]): Unit = {

    val fileBasedAddressBookService = new FileBasedAddressBookService("./src/test/resources/address-book.csv")
    val contact = fileBasedAddressBookService.findByName("Sarah Stone")
    println("========== Find By Name ==========================================")
    println(s"${contact.get}")
    println("========== Find By Gender ========================================")
    (fileBasedAddressBookService.findByGender(Gender.FEMALE)).foreach(println(_))
    println("========== Find Oldest ===========================================")
    println(s"${fileBasedAddressBookService.findOldest()}")
    println("========== Find Age Difference Between Bill and Paul =============")
    val bill = fileBasedAddressBookService.findByName("Bill McKnight").get
    val paul = fileBasedAddressBookService.findByName("Paul Robinson").get
    val ageDifference = fileBasedAddressBookService.findAgeDifference(bill,paul)
    println(s"Age Difference in Days between Bill and Paul = ${ageDifference}")
  }
}