package uk.co.gumtree

import java.util.Date

import org.joda.time.{PeriodType, DateTime, Period}

import scala.io.Source

/**
 * Scala equivalent to Java Enum
 */
object Gender extends Enumeration {
  type Gender = Value
  val MALE = Value("MALE")
  val FEMALE = Value("FEMALE")
}

import Gender._

case class Contact(name: String, dob: Date, gender: Gender)

/**
 * Implementation of AddressBook Services
 */
trait AddressBookService {

  // Structural type - can only be mixed in with Classes which implement contactsStream method.
  this: { def contactsStream(): Stream[Contact] } =>

  def findByName(name: String): Option[Contact] = {
    contactsStream().find(_.name == name)
  }

  def findByGender(gender: Gender): Stream[Contact] = {
    contactsStream().filter(_.gender == gender)
  }

  def findOldest(): Contact = {
    contactsStream().sortWith(_.dob.getTime < _.dob.getTime).head
  }

  def findAgeDifference(contact1: Contact, contact2: Contact): Integer = {
    val period: Period = new Period(new DateTime(contact1.dob.getTime, JODA_BRITISH_TIME_ZONE), new DateTime(contact2.dob.getTime, JODA_BRITISH_TIME_ZONE), PeriodType.dayTime)
    return Math.abs(period.getDays)
  }

}

/**
 *
 * @param filepath
 */
class FileBasedAddressBookService(filepath: String) extends AddressBookService {

  def contactsStream(): Stream[Contact] = {
    AddressBookFileParser.parseAddressBook(filepath).toStream
  }

}

/**
 * Singleton Object to parse File.
 */
object AddressBookFileParser {

  /**
   * Read the file and return a Stream of Contact's.
   * <p>
   * Concerns about performance if the file is big.. spark etc would be needed in this instance
   * would perhaps check size of file before attempting to parse.
   *
   * @param path
   * @return stream of contacts
   */
  def parseAddressBook(path: String): List[Contact] = {
    // val file: File = new File(path)
    // if(file.length()>)
    val bufferedSource = Source.fromFile(path)
    try {
      bufferedSource.getLines.map(parseLine(_)).flatten.toList
    } finally {
      bufferedSource.close()
    }
  }

  /*
   * Mapping function, using a simple parse.
   *
   * @param line format name,gender,dob. Comments start with #, blank lines and formatted files return None
   * @return Some[Contact] or None
   */
  def parseLine(line: String): Option[Contact] = {

    if(line.trim.isEmpty || line.trim.startsWith("#")) {
      None
    } else {
      val parts = line.split(",").map(_.trim)
      if (parts.length != 3) {
        None
      } else {
        try {
          // could have used Extractor - but not sure if it's best approach
          val name: String = parts(0)
          val gender: Gender = Gender.withName(parts(1).toUpperCase)
          val dob: Date = ADDRESS_BOOK_DATE_TIME_FORMATTER.parse(parts(2))
          Some(Contact(name, dob, gender))
        } catch {
          case _: Throwable => {
            println(s"Failed to parse line [$line] ignored")
            None
          }
        }
      }
    }
  }

}

