package uk.co.gumtree

import org.joda.time.{LocalDate, DateTime}
import org.scalatest.{Matchers, FlatSpec}
import uk.co.gumtree.Gender.Gender

/**
 * Created by tony_murphy on 02/08/15.
 */
class AddressBookFileParserSpec extends FlatSpec with Matchers {

  val midnight: DateTime = new LocalDate().toDateTimeAtStartOfDay(JODA_BRITISH_TIME_ZONE)

  behavior of "AddressBookFileParserSpec"

  it should "should read in lines from the file" in {
    val contacts: List[Contact] = AddressBookFileParser.parseAddressBook("./src/test/resources/address-book.csv")
    contacts should have length 5
  }

  it should "parse a String into a Contact" in {
    val name = "Bill McKnight"
    val gender: Gender = Gender.MALE
    val dob: String = midnight.toString("dd/MM/yy")
    val line: String = name + "," + gender.toString + "," + dob
    val contact: Option[Contact] = AddressBookFileParser.parseLine(line)
    contact.get.name shouldBe name
    contact.get.dob shouldBe midnight.toDate
    contact.get.gender shouldBe Gender.MALE
  }

  it should "handle empty line" in {
    val contact: Option[Contact] = AddressBookFileParser.parseLine(" ")
    contact shouldBe None
  }

  it should "handle line starting with a comment symbol" in {
    val contact: Option[Contact] = AddressBookFileParser.parseLine("#")
    contact shouldBe None
  }

  it should "handle line starting with a comment symbol not as first character" in {
    val contact: Option[Contact] = AddressBookFileParser.parseLine(" # ")
    contact shouldBe None
  }

  it should "handle line containing a comment" in {
    val contact: Option[Contact] = AddressBookFileParser.parseLine("#")
    contact shouldBe None
  }

  it should "handle case where line missing all necessary fields" in {
    val contact: Option[Contact] = AddressBookFileParser.parseLine("name,Female")
    contact shouldBe None
  }

  it should "handle case where data can't be parsed" in {
    val contact: Option[Contact] = AddressBookFileParser.parseLine("name,Female,??")
    contact shouldBe None
  }

  it should "parse and transform a String into a Contact" in {
    val contacts = AddressBookFileParser.parseAddressBook("./src/test/resources/address-book.csv")
    contacts should have length(5)
  }

}
