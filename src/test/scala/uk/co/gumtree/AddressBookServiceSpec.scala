package uk.co.gumtree

import org.joda.time.DateTime
import org.scalatest._

class AddressBookServiceSpec extends FlatSpec with Matchers with AddressBookService {


  val contactTony = Contact(name="tony",dob= (new DateTime(1977, 3, 16, 1, 1)).toDate, gender = Gender.MALE)
  val contactCat = Contact(name="cat",dob= (new DateTime(1985, 1, 15, 1, 1)).toDate, gender = Gender.FEMALE)


  def contactsStream(): Stream[Contact] = {
    val contacts: Seq[Contact] = Seq(contactTony, contactCat)
    contacts.toStream
  }

  behavior of "AddressBookService"

  it should "find contact by name" in {
    findByName(contactTony.name).get shouldBe contactTony
  }

  it should "fail to find non existing contact by name" in {
    findByName("so and so") shouldBe None
  }

  it should "find contacts by gender " in {
    val males = findByGender(Gender.MALE)
    males should have size(1)
    males.contains(contactTony) shouldBe true

    val females = findByGender(Gender.FEMALE)
    females should have size(1)
    females.contains(contactCat) shouldBe true
  }

  it should "find the oldest" in {
    findOldest() shouldBe contactTony
  }

  it should "find the age difference between contacts" in {
    findAgeDifference(contactTony, contactCat) shouldBe 2862
  }

}
